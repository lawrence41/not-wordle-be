use js_sys::{JsString};
use wasm_bindgen_test::*;

use crate::{Game};

#[wasm_bindgen_test]
fn test_initialization() {
    let game = setup_test();
    assert_ne!(game.get_answer(), "");
}

#[wasm_bindgen_test]
fn test_guess_logic() {
    // GIVEN we have an initialized game instance
    let mut game = setup_test();
    game.answer = "hello".to_owned();
    // WHEN an attempted word isn't in the word list
    // THEN it will return None
    let result: Option<[usize; 5]> = game.attempt_solve("bello");
    assert_eq!(result, None);
    //GIVEN a word LASSO
    //AND an answer HELLO
    //THEN the output should be [1,0,0,0,2]
    let result2: Option<[usize; 5]> = game.attempt_solve("lasso");
    assert_eq!(result2.unwrap(), [1,0,0,0,2]);
    //GIVEN a word LEVEL
    //AND answer   HELLO
    //THEN the output should be [1,2,0,0,1]
    let result3: Option<[usize; 5]> = game.attempt_solve("level");
    assert_eq!(result3.unwrap(), [1, 2, 0, 0, 1]);
    //GIVEN a word SSSSS
    //AND answer CESSF
    //THEN the output should be [0,0,2,2,0]
    game.answer = "cessf".to_owned();
    let result3: Option<[usize; 5]> = game.attempt_solve("sssss");
    assert_eq!(result3.unwrap(), [0, 0, 2, 2, 0]);


    //GIVEN a word CORES
    //AND answer CUFIC
    //THEN the output should be [2, 0, 0, 0, 0];
    game.answer = "cufic".to_owned();
    let result4: Option<[usize; 5]> = game.attempt_solve("cores");
    assert_eq!(result4.unwrap(), [2, 0, 0, 0, 0]);

    //GIVEN a word NAILS
    //AND answer PUPIL
    //THEN the output should be [2, 0, 0, 0, 0];
    game.answer = "cufic".to_owned();
    let result4: Option<[usize; 5]> = game.attempt_solve("cores");
    assert_eq!(result4.unwrap(), [2, 0, 0, 0, 0])


}

#[wasm_bindgen_test]
fn reset_game_test() {
    let game = Game::new(gen_js_string_list(vec!["hello", "world"]));
    let _ = game.attempt_solve(&game.answer);
}
fn setup_test() -> Game {
    Game::new(gen_js_string_list(vec!["hello", "jello", "match", "lasso", "level", "sssss", "cessf", "cores", "cufic"]))
}

fn gen_js_string_list(list: Vec<&str>) -> Box<[JsString]> {
    let vec_list: Vec<JsString> = list.iter().map(|v| JsString::from(*v)).collect();
    vec_list.into_boxed_slice()
}
