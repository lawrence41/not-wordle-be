use js_sys::{ JsString };
use rand;
use rand::{Rng, thread_rng};
use rand::rngs::ThreadRng;
use wasm_bindgen::prelude::*;

mod utils;
mod tests;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
extern {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub struct Dictionary {
    words: Box<[JsString]>,
    word_count: usize,
}

#[wasm_bindgen]
impl Dictionary {
    #[wasm_bindgen(constructor)]
    pub fn new(words: Box<[JsString]>) -> Self {
        let word_count = words.len();
        Dictionary {
            words,
            word_count,
        }
    }

    #[wasm_bindgen]
    pub fn get_word_count(&self) -> usize {
        self.word_count
    }

    #[wasm_bindgen]
    pub fn get_word(&self, i: usize) -> Option<String> {
        self.words.get(i).unwrap().as_string()
    }
}

#[wasm_bindgen]
pub struct Game {
    dictionary: Dictionary,
    rng: ThreadRng,
    answer: String,
    board_state: JsValue,
}

#[wasm_bindgen]
impl Game {
    #[wasm_bindgen(constructor)]
    pub fn new(word_list: Box<[JsString]>) -> Self {
        let dictionary = Dictionary::new(word_list);
        let mut rng = thread_rng();
        let answer_index = rng.gen_range(0..dictionary.words.len());
        let answer = dictionary.get_word(answer_index).unwrap();
        Game {
            dictionary,
            rng,
            answer,
            board_state: JsValue::from(""),
        }
    }


    #[wasm_bindgen]
    pub fn get_random_word_index(&mut self) -> usize {
        self.rng.gen_range(0..self.dictionary.words.len())
    }

    #[wasm_bindgen]
    pub fn new_game(&mut self) {
        let rng_index = self.get_random_word_index();
        self.answer = self.dictionary.get_word(rng_index).unwrap()
    }

    #[wasm_bindgen]
    pub fn get_answer(&self) -> String {
        self.answer.clone()
    }

    /**
    Returns an Uint32Array 
    **/
    #[wasm_bindgen]
    pub fn guess(&self, guess: JsString) -> Option<Box<[usize]>> {
        let guess_str = guess.as_string().unwrap();
        let result_option = self.attempt_solve(&guess_str);
        if let Some(result) = result_option {
            Some(vec![result[0], result[1], result[2], result[3], result[4]].into_boxed_slice())
        } else {
            None
        }
    }

    fn get_number_of_occurrences(&self, current_character: u8, answer_chars: &[u8]) -> usize {

        return answer_chars.iter()
            .filter(|answer_char| { **answer_char == current_character })
            .collect::<Vec<&u8>>()
            .len();
    }

    fn attempt_solve(&self, guess: &str) -> Option<[usize; 5]> {
        if !self.dictionary.words.contains(&JsString::from(guess)) {
            return None;
        }
        if self.answer.eq(guess) {
            Some([2, 2, 2, 2, 2])
        } else {
            let mut result = [0, 0, 0, 0, 0];
            let guess_chars = guess.as_bytes();
            let answer_chars = self.answer.as_bytes();

            for idx in 0..5 {
                let guess_c = guess_chars[idx] as char;
                let answer_c = answer_chars[idx] as char;
                if idx < 5 {
                    let current_character = guess_chars[idx];
                    let number_of_occurrences = self.get_number_of_occurrences(current_character, answer_chars);
                    if number_of_occurrences != 0 {
                        result[idx] = 0;
                    }
                    if guess_c == answer_c {
                        result[idx] = 2;
                    }
                }
            }


            for idx in 0..5 {
                if idx < 5 {
                    let guess_char = guess_chars[idx];

                    let mut number_of_letters_accounted_for = 0;
                    for iidx in 0..5 {
                        let result_state = result[iidx];

                        if iidx < 5 && guess_char == guess_chars[iidx] {
                            if result_state > 0 {
                                number_of_letters_accounted_for += 1;
                            }
                        }
                        let number_of_occurrences = self.get_number_of_occurrences(guess_char, answer_chars);

                        if number_of_letters_accounted_for < number_of_occurrences && result[idx] == 0 {
                            result[idx] = 1;
                        } else if result[idx] != 2 {
                            result[idx] = 0;
                        }
                    }


                };
            }

            Some(result)
        }
    }
}

